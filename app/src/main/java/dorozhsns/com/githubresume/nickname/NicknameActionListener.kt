package dorozhsns.com.githubresume.nickname

interface NicknameActionListener {

    fun onGenerateButtonClick()

    fun nicknameTextChanged(nickname: String)
}