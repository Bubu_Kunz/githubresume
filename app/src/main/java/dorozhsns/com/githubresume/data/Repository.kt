package dorozhsns.com.githubresume.data

import androidx.lifecycle.LiveData
import dorozhsns.com.githubresume.data.entity.User

interface Repository {

    fun getUser(nickname: String): LiveData<ResultObject<User>>
}