package dorozhsns.com.githubresume.data

import dorozhsns.com.githubresume.data.entity.User

interface NetworkService {

    suspend fun getUser(nickName: String): ResultObject<User>
}