package dorozhsns.com.githubresume.data

interface ConnectivityStatus {

    fun hasConnection() : Boolean
}