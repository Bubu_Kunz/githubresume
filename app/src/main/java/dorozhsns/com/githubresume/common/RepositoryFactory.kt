package dorozhsns.com.githubresume.common

import android.content.Context
import android.net.ConnectivityManager
import dorozhsns.com.githubresume.data.ConnectivityDispatcher
import dorozhsns.com.githubresume.data.GithubResumeRepository
import dorozhsns.com.githubresume.data.NetworkDataSource
import dorozhsns.com.githubresume.data.Repository

class RepositoryFactory {

    companion object {
        fun provideRepository (context: Context) : Repository {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
            val connectivityStatus = ConnectivityDispatcher.getInstance(connectivityManager)
            val networkDataSource = NetworkDataSource.getInstance(
                connectivityStatus)
            return GithubResumeRepository.getInstance(networkDataSource)
        }
    }
}